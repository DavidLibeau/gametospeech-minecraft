package com.gametospeech.gametospeechminecraft;

import com.gametospeech.gametospeechminecraft.mixin.AccessorHandledScreen;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.gametospeech.gametospeechminecraft.gui.ConfigScreen;
import com.gametospeech.gametospeechminecraft.gui.ConfigGui;
import com.gametospeech.gametospeechminecraft.keyboard.KeyboardController;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;

import com.gametospeech.gametospeechminecraft.config.Config;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;

import net.minecraft.client.util.InputUtil;
import net.minecraft.entity.Entity;
import net.minecraft.util.ActionResult;

import net.minecraft.block.entity.SignBlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.screen.slot.Slot;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import org.lwjgl.glfw.GLFW;

import static net.minecraft.server.command.CommandManager.*;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class GameToSpeechMinecraft implements ModInitializer {
    public static KeyBinding CONFIG_KEY;
    public static GameToSpeechMinecraft instance;
    public static NarratorPlus narrator;
    public static KeyboardController keyboardController;
    protected String lastData;
    public static String accessToken = "";
    public static String userId = "";

    @Override
    public void onInitialize() {
        instance = this;
        CONFIG_KEY = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.gametospeech.config",
                InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_N, "key.categories.gametospeech.general"));

        narrator = new NarratorPlus();
        keyboardController = new KeyboardController();
        System.setProperty("java.awt.headless", "false");

        lastData = "";

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (client.player == null)
                return;

            while (CONFIG_KEY.wasPressed()) {
                client.openScreen(new ConfigScreen(new ConfigGui(client.player)));
                return;
            }
            if (client.currentScreen != null && client.currentScreen instanceof AccessorHandledScreen) {
                Slot hovered = ((AccessorHandledScreen) client.currentScreen).getFocusedSlot();
                if (hovered != null && hovered.hasStack()) {
                    narrator.prefixAmount = String.valueOf(hovered.getStack().getCount()) + " ";
                } else {
                    narrator.prefixAmount = "";
                }
            }
            if (client.currentScreen == null || !(client.currentScreen instanceof AccessorHandledScreen)) {
                HitResult hit = client.crosshairTarget;
                switch (hit.getType()) {
                    case MISS:
                        // nothing near enough
                        break;
                    case BLOCK:
                        try {
                            BlockHitResult blockHit = (BlockHitResult) hit;
                            BlockPos blockPos = blockHit.getBlockPos();
                            BlockState blockState = client.world.getBlockState(blockPos);

                            Block block = blockState.getBlock();
                            if (!block.equals(narrator.lastBlock) || !blockPos.equals(narrator.lastBlockPos)) {
                                narrator.lastBlock = block;
                                narrator.lastBlockPos = blockPos;
                                String output = "";
                                if (Config.readBlocksEnabled()) {
                                    output += block.getName().getString();
                                }
                                if (blockState.toString().contains("sign") && Config.readSignsContentsEnabled()) {
                                    try {
                                        SignBlockEntity signentity = (SignBlockEntity) client.world
                                                .getBlockEntity(blockPos);
                                        output += " says: ";
                                        output += "1: " + signentity.getTextOnRow(0).getString() + ", ";
                                        output += "2: " + signentity.getTextOnRow(1).getString() + ", ";
                                        output += "3: " + signentity.getTextOnRow(2).getString() + ", ";
                                        output += "4: " + signentity.getTextOnRow(3).getString();
                                    } finally {
                                    }
                                }
                                if (!output.equals("") && !lastData.equals(output)) {
                                    lastData = output;
                                    sendToServer(output);
                                }
                            }
                        } finally {
                        }
                        break;
                    case ENTITY:
                        // Entity in range
                        EntityHitResult entityHit = (EntityHitResult) hit;
                        Entity entity = entityHit.getEntity();
                        System.out.println(entity);
                        break;
                }
            }
        });

        CommandRegistrationCallback.EVENT.register((dispatcher, dedicated) -> {
            dispatcher.register(literal("gametospeech")
                .then(literal("help").executes(ctx -> {
                    ctx.getSource().sendFeedback(new LiteralText("GameToSpeech help."), false);
                    //ctx.getSource().sendMessage(new TranslatableText("coin.flip.heads"));

                    System.out.println("foo");
                    return 1;
                    })
                )
                .then(literal("connect").then(argument("loginKey", StringArgumentType.word()).executes(ctx -> {
                    HttpClient client = HttpClient.newHttpClient();
                    try {
                        String loginKey = URLEncoder.encode(StringArgumentType.getString(ctx, "loginKey"), StandardCharsets.UTF_8.toString());
                        System.out.println("Connecting with loginKey="+loginKey);
                        HttpResponse<String> response = client.send(HttpRequest.newBuilder()
                            .uri(URI.create("https://api.gametospeech.com/connect/?game=minecraft-fabricmod&login_key=" + loginKey))
                            .build(), HttpResponse.BodyHandlers.ofString());
    
                        System.out.println("Java HttpClient GET: " + response.body());
                        if(response.statusCode() == 200){
                            Gson gson = new Gson();
                            JsonObject body = gson.fromJson(response.body(), JsonObject.class);
                            this.accessToken = body.get("access_token").getAsString();
                            this.userId = body.get("user_id").getAsString();
                            ctx.getSource().sendFeedback(new TranslatableText("gametospeech.connect.success"), false);
                            return 1;
                        }
                        ctx.getSource().sendFeedback(new TranslatableText("gametospeech.connect.error"), false);
                        ctx.getSource().sendFeedback(new LiteralText("Error code: " + response.statusCode()), false);
                        return 0;
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                        ctx.getSource().sendFeedback(new TranslatableText("gametospeech.connect.error"), false);
                        return 0;
                    }
                })))
            );
        });

        System.out.println("GameToSpeech mod initialized");
    }

    public void sendToServer(String data){
        if(this.userId.length() > 0 & this.accessToken.length() > 0){
            HttpClient client = HttpClient.newHttpClient();
            try {
                String json =  "{\"category\":\"raw\",\"value\":\""+data+"\"}";
                json = URLEncoder.encode(json, StandardCharsets.UTF_8.toString());
                HttpResponse<String> response = client.send(HttpRequest.newBuilder()
                        .uri(URI.create("https://api.gametospeech.com/minecraft-fabricmod/"+this.userId+"/?data=" + json + "&access_token=" + this.accessToken))
                        .build(), HttpResponse.BodyHandlers.ofString());
    
                System.out.println("Java HttpClient GET: " + response.body());
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


