# GameToSpeech Minecraft Prototype

This project is a prototype of a mod aim to generate audio description for live streamed Minecraft's spectators.

## Fork

This mod is a fork of the [Accessibility Plus mod](https://github.com/LuisSanchez-Dev/AccessibilityPlus) (MIT).